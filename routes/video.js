const express = require('express');
const router = express.Router();

// importar el modelo video

const PLaylist = require('../models/video');
const {verificarAuth,verificarAdministrador,verificarAuthKid} = require('../middlewares/autenticacion')

// Agregar una nota
router.post('/nuevo-video', verificarAuth, async(req, res) => {
  const body = req.body;  
   body.usuarioId = req.usuario._id;

  try {
    const videoDB = await PLaylist.create(body);
    res.status(200).json(videoDB); 
  } catch (error) {
    return res.status(500).json({
      mensaje: 'Ocurrio un error',
      error
    })
  }
});


// Get con parámetros
router.get('/video/:id', async(req, res) => {
    const _id = req.params.id;
    try {
      const videoDB = await PLaylist.findOne({_id});
      res.json(videoDB);
    } catch (error) {
      return res.status(400).json({
        mensaje: 'Ocurrio un error',
        error
      })
    }
  });
  
  // Get con todos los documentos
  router.get('/video',verificarAuth,async(req, res) => {

    const usuarioId = req.usuario._id;
    try {
      const videoDb = await PLaylist.find({usuarioId});
      res.json(videoDb);
    } catch (error) {
      return res.status(400).json({
        mensaje: 'Ocurrio un error',
        error
      })
    }
  });

   // Get con todos los documentos
   router.get('/videokid',verificarAuthKid,async(req, res) => {

   const usuarioId = req.usuario.usuarioId; //este debe ser validado de la base de datos
    try {
      const videoDb = await PLaylist.find({usuarioId});
      res.json(videoDb);
    } catch (error) {
      return res.status(400).json({
        mensaje: 'Ocurrio un error',
        error
      })
    }
  });


// Delete eliminar una nota
router.delete('/video/:id', async(req, res) => {
  const _id = req.params.id;

  try {
  const videoDb = await PLaylist.findByIdAndRemove({_id});
 
  if(!videoDb){

    return res.status(400).json({
      mensaje: 'No se encontró el id indicado',
      error
    })
  }
  res.json(videoDb);

 
    
  } catch (error) {
    return res.status(400).json({
      mensaje: 'Ocurrio un error',
      error
    })
  }
 
});


// Put actualizar una nota
router.put('/video/:id', async(req, res) => {
  const _id = req.params.id;
  const body = req.body;
  try {
    const videoDb = await PLaylist.findByIdAndUpdate(
      _id,
      body,
      {new: true});
    res.json(videoDb);  
  } catch (error) {
    return res.status(400).json({
      mensaje: 'Ocurrio un error',
      error
    })
  }
});

// Get con parámetros
router.get('/videofilter/:nombre', async(req, res) => {
  const nombre = req.params.nombre;
  try {
    const videoDB = await PLaylist.findOne({nombre});
    res.json(videoDB);
  } catch (error) {
    return res.status(400).json({
      mensaje: 'Ocurrio un error',
      error
    })
  }
});

// Exportamos la configuración de express app
module.exports = router;