//import express from 'express'
///const mongoose = require('mongoose');
///mongoose.Promise = global.Promise;
///const db = 'mongodb://localhost:27017/myapp';


var express = require('express');
var router = express.Router();
const _ = require('underscore');
var nodemailer = require("nodemailer");
var rand,mailOptions,host,link;
rand = 0;
host="";
mailOptions="";
var usuarioDB;
usuarioDB="";
// Or using promises
///mongoose.connection.openUri(db).then(
  /** ready to use. The `mongoose.connect()` promise resolves to mongoose instance. */
  ///() => { console.log('Conectado a MongoDB') },
  /** handle initial connection error */
  ///err => { console.log(err) }
///);


//const from,to,subject,text;

//const {verificarAuth} = require('../middlewares/autenticacion.js');

// Importamos modelo Tarea
const User = require('../models/user');
const {verificarAuth,verificarAdministrador} = require('../middlewares/autenticacion')

// Hash Contraseña
const bcrypt = require('bcrypt');
const saltRounds = 10;

router.post('/nuevo-usuario', async (req, res) => {

  const body = {
    nombre: req.body.nombre,
    email: req.body.email,
    fecha: req.body.fecha,
    pais: req.body.pais,
    telefono: req.body.telefono,
    role: req.body.role
  }
  
  //encriptar contraseña
  body.pass = bcrypt.hashSync(req.body.pass, saltRounds);
  body.confirmpass = bcrypt.hashSync(req.body.confirmpass, saltRounds);//nuevo

  //validar si las contraseñas son iguales
   
if(req.body.pass!=req.body.confirmpass){  //nuevo
  return res.status(400).json({  //nuevo
    mensaje: 'Passwords do not match', //nuevo
  }); //nuevo
}

//validar campos vacidos

    if(req.body.nombre =='' || req.body.email == '' || req.body.pass =='' || req.body.confirmpass == '' 
    || req.body.fecha == '' || req.body.telefono == '' || req.body.pais == ''){
      return res.status(400).json({  //nuevo
        mensaje: 'The fields are mandatory', //nuevo
      }); //nuevo

    }

  //Calcular edad
 
  var hoy = new Date();
  var cumpleanos = new Date(req.body.fecha);
  var edad = hoy.getFullYear() - cumpleanos.getFullYear();
  var m = hoy.getMonth() - cumpleanos.getMonth();

  if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
      edad--;
  }
 //validadar si es mayor de 18 años
  if (edad<18){
    return res.status(400).json({  //nuevo
      mensaje: 'You are not 18 or older', //nuevo
    });

  }
    

  try {

    var smtpTransport = nodemailer.createTransport({
      service: "Gmail",
      auth: {
          user: 'tubekidsweb2020@gmail.com',
          pass: 'tubekids2020'
      }
  });
 // var rand,mailOptions,host,link;
  /*------------------SMTP Over-----------------------------*/
  //  user: 'clemmie.mills@ethereal.email',
          //pass: 'nFwMCfgt8xRt6cC8tF'
  /*------------------Routing Started ------------------------*/
  
  //app.get('/',function(req,res){
    //res.sendfile('index.html');
  //});
  
 
    global.rand=Math.floor((Math.random() * 100) + 54);
    global.host=req.get('host');
    link="http://"+req.get('host')+"/api"+"/verify?id="+global.rand;
    mailOptions={
      to : req.body.email,
      subject : "Please confirm your Email account",
      html : "Hello,<br> Please Click on the link to verify your email.<br><a href="+link+">Click here to verify</a>"	
    }
    console.log(mailOptions);
    smtpTransport.sendMail(mailOptions, function(error, response){
        if(error){
            console.log(error);
      res.end("error");
     }else{
            console.log("Message sent: " + response.message);
      res.end("sent");
         }
  });

  
    usuarioDB = await User.create(body);
    return res.json(usuarioDB);

     

  } catch (error) {
    return res.status(500).json({
      mensaje: 'Ocurrio un error',
      error
    });
  }
 
  

});

router.get('/verify',function(req,res){
  
  console.log(req.protocol+"://"+req.get('host'));
  if((req.protocol+"://"+req.get('host'))==("http://"+global.host))
  {
    console.log("Domain is matched. Information is from Authentic email");
    if(req.query.id==global.rand)
    {
      console.log(global.rand+req.query.id );
      console.log("email is verified");

    // console.log(res);

     //usuario tiene que pasar a true, necesito saber el usuario

       
     console.log("el modo es" +" " + usuarioDB.activo);
     console.log("el modo es" +" " + usuarioDB._id);
 
    
     
// actualizar parametro de activo
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/myapp";

 MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("myapp");
  var myquery = { _id: usuarioDB._id };
  var newvalues = {$set: {activo: true} };
  dbo.collection("users").updateOne(myquery, newvalues, function(err, res) {
    if (err) throw err;
    console.log("1 document updated");
    db.close();
  });
});
     
     
 

      
      
      //res.end("<h1>Email "+mailOptions.to+" is been Successfully verified</h1>");

      res.redirect("http://localhost:8080/login")
      
      


      
    }
    else
    {
      console.log("email is not verified");
      //res.end("<h1>Bad Request</h1>");
      res.redirect("http://localhost:8080/");
    }
  }
  else
  {

    console.log("el modo es" + usuarioDB.id);
    
    
    res.redirect("http://localhost:8080/");
  }
  });




router.put('/usuario/:id', [verificarAuth,verificarAdministrador], async(req, res) => {

  let id = req.params.id;
  //parametros que el usuario puede modificar
  let body = _.pick(req.body, ['nombre', 'email', 'activo', 'pass']);
  if(body.pass){
    body.pass = bcrypt.hashSync(req.body.pass, saltRounds);
  }

  try {
    // {new:true} nos devuelve el usuario actualizado
    const usuarioDB = await User.findByIdAndUpdate(id, body, {new: true, runValidators: true});

    return res.json(usuarioDB);

  } catch (error) {
    return res.status(400).json({
      mensaje: 'Ocurrio un error',
      error
    })
  }

});




module.exports = router;


