const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const User = require('../models/userkid');
const bcrypt = require('bcrypt');
const saltRounds = 10;


router.post('/', async(req, res) => {
    //res.json({mensaje: 'Funciona!'})

    const body = req.body;

    try {

        // Buscamos email en DB
      const usuarioDB = await User.findOne({usuario: body.usuario});
         
       // Evaluamos si existe el usuario en DB
    if(!usuarioDB){
        return res.status(400).json({
          mensaje: 'Invalid user or PIN'
        });
      }

       // Evaluamos la contraseña correcta
    if(!usuarioDB.pin){
        return res.status(400).json({
          mensaje: 'Invalid user or PIN',
        });
      }

      let acces = jwt.sign({
        data: usuarioDB
      }, 'secret', { expiresIn: 60 * 60 * 24 * 30}) // Expira en 30 días 60*60 es una hora
    
           // Pasó las validaciones
        return res.json({
            usuarioDB,
            acces
          })

        
    } catch (error) {
        return res.status(400).json({
            mensaje: 'Ocurrio un error',
            error
        })
    }
  });
  
  module.exports = router;