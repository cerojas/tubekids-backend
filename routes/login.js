var sessionstorage = require('sessionstorage');

const express = require('express');
var twilio = require('twilio');
const router = express.Router();
const jwt = require('jsonwebtoken');
const User = require('../models/user');
const bcrypt = require('bcrypt');
const saltRounds = 10;
//var codigo;
//codigo = 0;


router.post('/', async(req, res) => {
    //res.json({mensaje: 'Funciona!'})

    const body = req.body;

    try {

        // Buscamos email en DB
      const usuarioDB = await User.findOne({email: body.email});
         
       // Evaluamos si existe el usuario en DB
    if(!usuarioDB){
        return res.status(400).json({
          mensaje: 'Invalid username or password'
        });
      }

      
    // Evaluamos la contraseña correcta
    if( !bcrypt.compareSync(body.pass, usuarioDB.pass) ){
        return res.status(400).json({
          mensaje: 'Invalid username or password',
        });
      }

      // Evaluamos si usuario esta activo 

      if( usuarioDB.activo==false) {
        return res.status(400).json({
          mensaje: 'The user is not active',
        });
      }

      


      //Generar PIN 

     const codigo=Math.floor((Math.random() * 100000) + 54);

    // sessionstorage.setItem('codigo', codigo)

     //console.log('item set:', sessionstorage.getItem('codigo'))

      //sessionStorage.setItem('codigo', codigo);


      //Enviar PIN por SMS

      // Create a new REST API client to make authenticated requests against the
// twilio back end
var client = new twilio("ACe92e924e3b2974e8324b0c372606e60d", "7bbe340d6ac5a7bfb68dda0d55b67e38");

// Pass in parameters to the REST API using an object literal notation. The
// REST client will handle authentication and response serialzation for you.

client.messages.create({
    to:'506'+ usuarioDB.telefono,
    from:'5596343112',
    body:'Este es su pin para ingresar' + " " + codigo
}, function(error, message) {
    // The HTTP request to Twilio will run asynchronously. This callback
    // function will be called when a response is received from Twilio
    // The "error" variable will contain error information, if any.
    // If the request was successful, this value will be "falsy"
    if (!error) {
        // The second argument to the callback will contain the information
        // sent back by Twilio for the request. In this case, it is the
        // information about the text messsage you just sent:
        console.log('Success! The SID for this SMS message is:');
        console.log(message.sid);

        console.log('Message sent on:');
        console.log(message.dateCreated);
    } else {
        console.log('Oops! There was an error.');
    }
});


      // Generar Token
   let token = jwt.sign({
    data: usuarioDB
  }, 'secret', { expiresIn: 60 * 60 * 24 * 30}) // Expira en 30 días 60*60 es una hora

       // Pasó las validaciones
    return res.json({
        usuarioDB,
        token,
        codigo
      })
        
    } catch (error) {
        return res.status(400).json({
            mensaje: 'Ocurrio un error',
            error
        })
    }
  });
  
  module.exports = router;