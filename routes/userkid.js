const express = require('express');
const router = express.Router();

// importar el modelo video

const Userkid = require('../models/userkid');
const {verificarAuth,verificarAdministrador} = require('../middlewares/autenticacion')

// Agregar una nota
router.post('/nuevo-usuariokid', verificarAuth, async(req, res) => {
  const body = req.body;  
  body.usuarioId = req.usuario._id;

  try {
    const userkidDB = await Userkid.create(body);
    res.status(200).json(userkidDB); 
  } catch (error) {
    return res.status(500).json({
      mensaje: 'Ocurrio un error',
      error
    })
  }
});


// Get con parámetros
router.get('/usuarioKid/:id', async(req, res) => {
    const _id = req.params.id;
    try {
      const userkidDB = await Userkid.findOne({_id});
      res.json(userkidDB);
    } catch (error) {
      return res.status(400).json({
        mensaje: 'Ocurrio un error',
        error
      })
    }
  });
  
  // Get con todos los documentos
  router.get('/usuarioKid',verificarAuth,async(req, res) => {

    const usuarioId = req.usuario._id;
    try {
      const userKidDb = await Userkid.find({usuarioId});
      res.json(userKidDb);
    } catch (error) {
      return res.status(400).json({
        mensaje: 'Ocurrio un error',
        error
      })
    }
  });


// Delete eliminar una nota
router.delete('/usuarioKid/:id', async(req, res) => {
  const _id = req.params.id;

  try {
  const userKidDb = await Userkid.findByIdAndRemove({_id});
 
  if(!userKidDb){

    return res.status(400).json({
      mensaje: 'No se encontró el id indicado',
      error
    })
  }
  res.json(userKidDb);

 
    
  } catch (error) {
    return res.status(400).json({
      mensaje: 'Ocurrio un error',
      error
    })
  }
 
});


// Put actualizar una nota
router.put('/usuarioKid/:id', async(req, res) => {
  const _id = req.params.id;
  const body = req.body;
  try {
    const userKidDb = await Userkid.findByIdAndUpdate(
      _id,
      body,
      {new: true});
    res.json(userKidDb);  
  } catch (error) {
    return res.status(400).json({
      mensaje: 'Ocurrio un error',
      error
    })
  }
});

// Exportamos la configuración de express app
module.exports = router;