const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const path = require('path');


const app = express();

//conexion a DB

const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const uri = 'mongodb://localhost:27017/myapp';


// Or using promises
mongoose.connection.openUri(uri).then(
    /** ready to use. The `mongoose.connect()` promise resolves to mongoose instance. */
    () => { console.log('Conectado a MongoDB') },
    /** handle initial connection error */
    err => { console.log(err) }
  );

// Middleware
app.use(morgan('tiny'));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// app.use(express.static(path.join(__dirname, 'public')));

// Rutas
app.use('/api', require('./routes/nota'));
app.use('/api', require('./routes/video'));
app.use('/api', require('./routes/userkid'));
app.use('/api', require('./routes/user'));
app.use('/api/login', require('./routes/login'));
app.use('/api/loginkid', require('./routes/loginkid'));
app.use('/api/logintwo', require('./routes/logintwo'));
//app.use('/verify', require('./routes/verify'));

//app.get('/', (req, res) => {
  //res.send('Hello World!');
//});

//app.use("/nota", require("./routes/nota"));

// Usamos las rutas
//app.use('/', indexRouter);
//app.use('/users', usersRouter);
//app.use('/login', require('./routes/login'));

// Middleware para Vue.js router modo history
const history = require('connect-history-api-fallback');
app.use(history());
app.use(express.static(path.join(__dirname, 'public')));

app.set('puerto', process.env.PORT || 1000);
app.listen(app.get('puerto'), () => {
  console.log('Example app listening on port'+ app.get('puerto'));
});


  