
const jwt = require('jsonwebtoken');

const verificarAuth = (req, res, next) => {

   // res.json({
       // mensaje : "dentro del middleware"
  //  })

     //Leer headers
    let token = req.get('token');
  
    jwt.verify(token, 'secret', (err, decoded) => {

        if(err) {
          return res.status(401).json({
            mensaje: 'Error de token',
        
          })
        }
    
        // Creamos una nueva propiedad con la info del usuario
        req.usuario = decoded.data; //data viene al generar el token en login.js
        next();
    
      });
    
    }
  
    const verificarAdministrador = (req, res, next) => {

        const rol = req.usuario.role
        
        if(rol === 'ADMIN'){
            next();
            
            } else{
                return res.status(401).json({
                mensaje: 'Error de token',
            })
        }
    }

    const verificarAuthKid = (req, res, next) => {

      // res.json({
          // mensaje : "dentro del middleware"
     //  })
   
        //Leer headers
       let acces = req.get('acces');
     
       jwt.verify(acces, 'secret', (err, decoded) => {
   
           if(err) {
             return res.status(401).json({
               mensaje: 'Error de acces',
           
             })
           }
       
           // Creamos una nueva propiedad con la info del usuario
           req.usuario = decoded.data; //data viene al generar el token en login.js
           next();
       
         });
       
       }

    
  
  
  module.exports = {verificarAuth, verificarAdministrador,verificarAuthKid};