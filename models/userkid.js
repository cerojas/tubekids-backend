const mongoose = require('mongoose');


const Schema = new mongoose.Schema({
  nombre: {type: String, required: [true, 'Nombre obligatorio']},
  usuario: String,
  edad: String,
  pin: String,
  usuarioId: String,
  date:{type: Date, default: Date.now}

});

// Convertir a modelo


module.exports = mongoose.model('Userkid', Schema);