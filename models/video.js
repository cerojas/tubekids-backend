const mongoose = require('mongoose');


const Schema = new mongoose.Schema({
  nombre: {type: String, required: [true, 'Nombre obligatorio']},
  video:  {type: String, required: [true, 'Link obligatorio']}, 
  usuarioId: String,
  date:{type: Date, default: Date.now}

});

// Convertir a modelo


module.exports = mongoose.model('PLaylist', Schema);