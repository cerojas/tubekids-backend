const mongoose = require('mongoose');


const Schema = new mongoose.Schema({
  nombre: {type: String, required: [true, 'Nombre obligatorio']},
  descripcion: String,
  usuarioId: String,
  date:{type: Date, default: Date.now},
  activo: {type: Boolean, default: true}
});

// Convertir a modelo


module.exports = mongoose.model('Nota', Schema);


